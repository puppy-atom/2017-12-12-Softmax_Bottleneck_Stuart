# *Breaking the Softmax Bottleneck: A High-Rank RNN Language Model* presented by Stuart Axelbrooke

ATOM is meeting on Tuesday, 12th December, 6:30pm, at Galvanize!  

Join us for a highly participative discussion !  

The main paper to discuss is “Breaking the Softmax Bottleneck: A High-Rank RNN Language Model” (https://arxiv.org/abs/1711.03953). The paper has very interesting results, but I am guessing given the somewhat rarity of people that are interested in NLP, some amount of time getting people up to speed on “NLP with neural nets, whatupwitdat” (and from that, sequence to sequence learning) would be a good idea. The original seq2seq paper is pretty good context (https://arxiv.org/abs/1409.3215), but given the quality of the intro in the paper under discussion, people may not need it.  

Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.  

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !  

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.  

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !
